## **fiche professeur** :

**Thématique** : La structure de base d'un ordinateur

**Notions liées** : ordinateur, clavier, sourie, imprimante, carte mère, processeur, carte mémoire, périphérique

**Résumé de l’activité** : Situation problème (Dans chaque groupe, un ordinateur démonté doit être réparé )

**Objectifs** : Découverte les composants de l'ordinateur. Savoir le rôle de chaque composant. L'emplqcement de chaque composant.

**Durée de l’activité** : 40min

**Forme de participation** : En groupe

**Matériel nécessaire** : ordinateurs et des petits tournevis ou tournevis cruciforme

**Préparation** : Aucune

**Autres références** : [lien](https://www.shutterstock.com/fr/search/composants+d+ordinateur).

**Fiche élève cours** :

**Fiche élève activité** :
