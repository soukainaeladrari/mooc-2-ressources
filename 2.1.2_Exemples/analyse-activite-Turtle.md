## Analyse Module turtle pour introduire python
- Lien: [pdf](filetmpactivitesturtl-epdf)
- Objectif:
Réaliser avec python et la bibliothèque turtle des figures
    - Introduire python
    - Introduire la sytntaxe python
    - Affectation des variables
    - les conditions
    - Boucle
    - Concept de fonction
- Pré-requis:
Pas de pré-requis(première activité en programmation)
- Durée de l'activité:
1h 30min
- Exercices cibles:
    - EXercices (Séance 1):
      La comprhénsion de l'algorithme
    - Exercices (Scéance 2) :
      Concepts de fonction condition boucle
- Description de la séance:

  **partie 1:**
Présentation d'une mise en situation (5min) pour savoir le role et l'intéret du bloc-note
pour écrire un algorithme. Présentation de l'intérpréteur Python avec des simples exemples (lire/afficher/affectation/calculer). Mise en pratique de Python avec un exercice de calcul. Vérification que tout le monde ait compris. Les élèves commencent à lire l'exercice et à executer les première lignes. les élèves sont en autonomie en travaillent en groupe de deux. Le prof passe pour aider les élèves et pour voir leurs propositions. Envoyer le cours
  
  **partie 2 (classes inversée):**
Présenter les exercices à réaliser. Le prof aide en cas de besoin. Travail en groupe et enfin faire une synthèse pour bien expliquer les concepts aux élèves.
